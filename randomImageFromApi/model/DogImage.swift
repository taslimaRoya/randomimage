//
//  DogImage.swift
//  randomImageFromApi
//
//  Created by Taslima Roya on 7/24/19.
//  Copyright © 2019 Taslima Roya. All rights reserved.
//

import Foundation
struct DogImage:Codable {
    let status:String
    let message:String
}
