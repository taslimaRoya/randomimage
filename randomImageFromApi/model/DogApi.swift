//
//  DogApi.swift
//  randomImageFromApi
//
//  Created by Taslima Roya on 7/23/19.
//  Copyright © 2019 Taslima Roya. All rights reserved.
//

import Foundation
class DogApi{
    enum EndPoint:String{
        case randomImageFromAllDogsCollection="https://dog.ceo/api/breeds/image/random"
        
        var url:URL{
            return URL(string: self.rawValue)!
        }
    }
}
