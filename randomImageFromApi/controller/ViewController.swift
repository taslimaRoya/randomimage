//
//  ViewController.swift
//  randomImageFromApi
//
//  Created by Taslima Roya on 7/23/19.
//  Copyright © 2019 Taslima Roya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
       let randomImageEndpoint = DogApi.EndPoint.randomImageFromAllDogsCollection.url
        let task=URLSession.shared.dataTask(with: randomImageEndpoint) { (data, response, error) in
            guard let data=data else{
                return
            }
            print(data)
           // do{
            //let json=try JSONSerialization.jsonObject(with: data, options: []) as!
               // [String:Any]
           // let url=json["message"] as!String
               // print(url)
       // } catch {
           // print(error)
       // }
       
        let decoder=JSONDecoder()
        let imageData = try! decoder.decode(DogImage.self, from: data)
            print(imageData)
            guard let imageURL = URL(string: imageData.message)else{
                return
            }
            let task=URLSession.shared.dataTask(with: imageURL, completionHandler: { (data, response, error) in
                guard let data=data else{
                    return
                }
                let downLoadedImage=UIImage(data: data)
                DispatchQueue.main.async {
                    self.imageView.image=downLoadedImage
                }
            })
            task.resume()
        
        }
        task.resume()
    }


}

